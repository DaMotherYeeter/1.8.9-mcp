def patch(patchfile, errorfile):
    f = open(errorfile, 'a')
    f.truncate(0)
    f.close()
    with open(patchfile) as f:
        with open(errorfile, 'w') as f1:
            for line in f:
                f1.write(line)